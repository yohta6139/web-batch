package com.example.webbatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebBatchApplication.class, args);
	}

}
